import { IAsset, IChef, ITagsCollection, ISys } from "./common";

export interface IRecipe {
  title: string;
  photo: IAsset;
  tagsCollection: ITagsCollection;
  description: string;
  chef: IChef | null;
  sys: ISys;
}
