export interface ISys {
  id: string;
}

export interface IAsset {
  url: string;
}

export interface ITag {
  name: string;
  sys: ISys;
}

export interface ITagsCollection {
  items: ITag[];
}

export interface IChef {
  name: string;
}
