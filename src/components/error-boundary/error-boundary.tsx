import React from "react";
import {
  IErrorBoundaryProps,
  IErrorBoundaryState,
} from "./error-boundary.types";

export class ErrorBoundary extends React.Component<
  IErrorBoundaryProps,
  IErrorBoundaryState
> {
  state: IErrorBoundaryState = {
    error: null,
  };

  static getDerivedStateFromError(error: Error) {
    return { error: error };
  }

  componentDidCatch(error: Error | null, errorInfo: React.ErrorInfo | null) {
    // log the error to an error reporting service
    console.error({ error, errorInfo });
  }

  render() {
    if (this.state.error) {
      return this.props.replaceTo || <h1>Something went wrong.</h1>;
    }

    return this.props.children;
  }
}
