export interface IErrorBoundaryProps {
  children: React.ReactNode;
  replaceTo?: React.ReactNode;
}

export interface IErrorBoundaryState {
  error: Error | null;
}
