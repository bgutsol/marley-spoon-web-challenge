import { IRecipe } from "../../types/recipe";

export interface IRecipeDetailsProps {
  recipeId: string;
}

export interface IRecipeDetailsData {
  recipe: IRecipe;
}

export interface IRecipeDetailsVars {
  id: string;
}
