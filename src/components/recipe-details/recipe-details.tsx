import React from "react";
import { useQuery } from "@apollo/client";
import { GET_RECIPE_DETAILS } from "./recipe-details.queries";
import {
  IRecipeDetailsProps,
  IRecipeDetailsData,
  IRecipeDetailsVars,
} from "./recipe-details.types";
import {
  RecipesDetailsContainer,
  RecipesDetailsImgWrapper,
  RecipesDetailsImg,
  RecipesDetailsInfo,
  RecipesDetailsTitle,
  RecipesDetailsTagsList,
  RecipesDetailsTagsItem,
  RecipesDetailsDescription,
  RecipesDetailsSkeleton,
  RecipesDetailsChefName,
} from "./recipe-details.styles";
import { Tag } from "../tag";
import { SkeletonPlaceholder } from "../skeleton-placeholder";
import { ErrorInformer } from "../error-informer";
import ReactMarkdown from "react-markdown";

export const RecipeDetails: React.FC<IRecipeDetailsProps> = (props) => {
  console.log({ recipeId: props.recipeId });
  const { loading, data, error } = useQuery<
    IRecipeDetailsData,
    IRecipeDetailsVars
  >(GET_RECIPE_DETAILS, {
    variables: {
      id: props.recipeId,
    },
  });

  if (loading) {
    return (
      <RecipesDetailsContainer>
        <RecipesDetailsSkeleton>
          <SkeletonPlaceholder />
        </RecipesDetailsSkeleton>
      </RecipesDetailsContainer>
    );
  }

  if (error || !data) {
    return (
      <ErrorInformer title="Something went wrong. Try to reload the page" />
    );
  }

  const { recipe } = data;

  return (
    <RecipesDetailsContainer data-test-id="recipe">
      <RecipesDetailsImgWrapper>
        <RecipesDetailsImg src={recipe.photo.url} alt={recipe.title} />
      </RecipesDetailsImgWrapper>
      <RecipesDetailsInfo>
        <RecipesDetailsTitle>{recipe.title}</RecipesDetailsTitle>
        {recipe.tagsCollection.items.length > 0 && (
          <RecipesDetailsTagsList>
            {recipe.tagsCollection.items.map((item) => (
              <RecipesDetailsTagsItem key={item.sys.id}>
                <Tag label={item.name} />
              </RecipesDetailsTagsItem>
            ))}
          </RecipesDetailsTagsList>
        )}
        <RecipesDetailsDescription>
          <ReactMarkdown source={recipe.description} />
        </RecipesDetailsDescription>
        {recipe.chef && (
          <RecipesDetailsChefName>
            Shared with you by: {recipe.chef.name}
          </RecipesDetailsChefName>
        )}
      </RecipesDetailsInfo>
    </RecipesDetailsContainer>
  );
};
