import styled from "styled-components";

export const RecipesDetailsContainer = styled.div`
  overflow: hidden;
  background-color: #fff;
  border-radius: 4px;
  margin: 30px auto;
`;

export const RecipesDetailsImgWrapper = styled.div`
  position: relative;
  height: 0;
  padding-bottom: 45%;
`;

export const RecipesDetailsImg = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const RecipesDetailsInfo = styled.div`
  padding: 3rem 15px;
  text-align: left;
  max-width: 630px;

  @media (max-width: 768px) {
    padding: 2rem 15px;
  }
`;

export const RecipesDetailsTitle = styled.h2`
  font-size: 2.8rem;
  margin-bottom: 3rem;

  @media (max-width: 768px) {
    font-size: 2.2rem;
    margin-bottom: 2rem;
  }
`;

export const RecipesDetailsTagsList = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  margin: -5px;
`;

export const RecipesDetailsTagsItem = styled.div`
  margin: 5px;
`;

export const RecipesDetailsDescription = styled.div`
  margin-top: 3rem;
  font-size: 1.6rem;

  @media (max-width: 768px) {
    margin-top: 2rem;
  }
`;

export const RecipesDetailsChefName = styled.div`
  margin-top: 3rem;
  font-size: 1.6rem;

  @media (max-width: 768px) {
    margin-top: 2rem;
  }
`;

export const RecipesDetailsSkeleton = styled.div`
  height: 600px;
`;
