import { gql } from "@apollo/client";

export const GET_RECIPE_DETAILS = gql`
  query GetRecipeDetails($id: String!) {
    recipe(id: $id) {
      title
      photo {
        url
      }
      tagsCollection {
        items {
          name
          sys {
            id
          }
        }
      }
      description
      chef {
        name
      }
    }
  }
`;
