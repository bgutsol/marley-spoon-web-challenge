import { mount, shallow } from "enzyme";
import { MockedProvider } from "@apollo/client/testing";
import { RecipeDetails } from "./recipe-details";
import { GET_RECIPE_DETAILS } from "./recipe-details.queries";
import { updateWrapper } from "../../tests/utils";
import { recipesDetailsMock } from "./recipes-details.mocks";
import toJson from "enzyme-to-json";

const RECIPE_ID = "RECIPE_ID";

const mocks = [
  {
    request: {
      query: GET_RECIPE_DETAILS,
      variables: {
        id: RECIPE_ID,
      },
    },
    result: {
      data: recipesDetailsMock,
    },
  },
];

describe("<RecipesDetails/>", () => {
  it("renders the RecipesDetails", () => {
    shallow(
      <MockedProvider mocks={mocks}>
        <RecipeDetails recipeId={RECIPE_ID} />
      </MockedProvider>
    );
  });

  it("snapshot test for RecipesDetails", async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <RecipeDetails recipeId={RECIPE_ID} />
      </MockedProvider>
    );
    await updateWrapper(wrapper);
    const recipe = wrapper.find('div[data-test-id="recipe"]');
    expect(toJson(recipe)).toMatchSnapshot();
  });
});
