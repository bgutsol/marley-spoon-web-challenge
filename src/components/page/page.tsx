import React from "react";
import { Meta } from "../meta";
import { Header } from "../header";
import { PageContainer, PageInner, GlobalStyle } from "./page.styles";
import { ErrorBoundary } from "../error-boundary";
import { ErrorInformer } from "../error-informer";

export const Page: React.FC = (props) => (
  <>
    <GlobalStyle />
    <PageContainer>
      <Meta />
      <Header />
      <PageInner>
        <ErrorBoundary
          replaceTo={
            <ErrorInformer title="Something went wrong. Try to reload the page" />
          }
        >
          {props.children}
        </ErrorBoundary>
      </PageInner>
    </PageContainer>
  </>
);
