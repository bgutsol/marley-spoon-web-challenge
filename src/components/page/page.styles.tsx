import styled, { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  html {
    box-sizing: border-box;
    font-size: 62.5%;
  }

  *, *:before, *:after {
    box-sizing: inherit;
  }

  body {
    padding: 0;
    margin: 0;
    font-size: 1.6rem;
    font-family: -apple-system, system-ui, Roboto, 'Open Sans', Helvetica, Arial, sans-serif;
    line-height: 1.4;
    color: #222;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  p,h1,h2,h3,h4,ul{
      margin: 0;
  }

  ul{
    padding: 0;
  }

  img{
    max-width: 100%;
    height: auto;
  }
`;

export const PageContainer = styled.div`
  width: 100%;
  min-height: 100vh;
  background-color: #f6f6f6;
  display: flex;
  flex-direction: column;
`;

export const PageInner = styled.div`
  width: 100%;
  max-width: 1120px;
  margin: 0 auto;
  padding: 0 20px;
  flex-grow: 1;
`;
