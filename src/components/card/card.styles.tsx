import styled from "styled-components";

export const CardTitle = styled.h2`
  font-size: 1.8rem;
  color: #2d2926;
  transition: all 0.3s ease-in-out;
`;

export const CardContainer = styled.div`
  width: 100%;
  height: 100%;
  background-color: #fff;
  border-radius: 4px;
  overflow: hidden;

  &:hover ${CardTitle} {
    color: #47d7ac;
  }
`;

export const CardImgWrapper = styled.div`
  position: relative;
  height: 0;
  padding-bottom: 70%;
`;

export const CardImg = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const CardInfo = styled.div`
  padding: 15px;
  text-align: left;
`;
