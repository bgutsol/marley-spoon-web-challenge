export interface ICardProps {
  title: string;
  imgSrc: string;
}
