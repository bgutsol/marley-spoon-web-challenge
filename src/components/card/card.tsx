import React from "react";
import { ICardProps } from "./card.types";
import {
  CardContainer,
  CardImgWrapper,
  CardImg,
  CardInfo,
  CardTitle,
} from "./card.styles";

export const Card: React.FC<ICardProps> = (props) => {
  return (
    <CardContainer>
      <CardImgWrapper>
        <CardImg src={props.imgSrc} alt={props.title}></CardImg>
      </CardImgWrapper>
      <CardInfo>
        <CardTitle>{props.title}</CardTitle>
      </CardInfo>
    </CardContainer>
  );
};
