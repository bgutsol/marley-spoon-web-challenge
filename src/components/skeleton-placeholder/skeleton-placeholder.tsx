import React from "react";
import { SkeletonPlaceholderContainer } from "./skeleton-placeholder.styles";

export const SkeletonPlaceholder: React.FC = () => (
  <SkeletonPlaceholderContainer />
);
