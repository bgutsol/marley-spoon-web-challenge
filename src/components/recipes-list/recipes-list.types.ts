import { IRecipe } from "../../types/recipe";

type IRecipeListItem = Pick<IRecipe, "title" | "photo" | "sys">;

export interface IRecipesListData {
  recipeCollection: {
    items: IRecipeListItem[];
  };
}
