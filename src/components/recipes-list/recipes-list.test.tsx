import { mount, shallow } from "enzyme";
import { MockedProvider } from "@apollo/client/testing";
import { RecipesList } from "./recipes-list";
import { GET_RECIPES_LIST } from "./recipes-list.queries";
import { updateWrapper } from "../../tests/utils";
import { recipesListMock } from "./recipes-list.mocks";
import toJson from "enzyme-to-json";

const mocks = [
  {
    request: { query: GET_RECIPES_LIST },
    result: {
      data: recipesListMock,
    },
  },
];

describe("<RecipesList/>", () => {
  it("renders the RecipesList", () => {
    shallow(
      <MockedProvider mocks={mocks}>
        <RecipesList />
      </MockedProvider>
    );
  });

  it("renders the RecipesList and show fetch results", async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <RecipesList />
      </MockedProvider>
    );
    await updateWrapper(wrapper);

    const recipes = wrapper.find('div[data-test-id="recipe"]');
    expect(recipes).toHaveLength(4);
  });

  it("snapshot test for first RecipesList item", async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <RecipesList />
      </MockedProvider>
    );
    await updateWrapper(wrapper);

    const recipes = wrapper.find('div[data-test-id="recipe"]');
    expect(toJson(recipes.first())).toMatchSnapshot();
  });
});
