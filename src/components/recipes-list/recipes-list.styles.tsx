import styled from "styled-components";

export const RecipesListContainer = styled.div`
  width: 100%;
  max-width: 900px;
  margin: 20px auto;
  display: flex;
  flex-wrap: wrap;
`;

export const RecipesListItem = styled.div`
  width: 50%;
  padding: 10px;

  @media (max-width: 768px) {
    width: 100%;
    padding: 10px 0;
  }
`;

export const RecipesListLink = styled.a`
  display: block;
  height: 100%;
  text-decoration: none;
`;

export const RecipesListCardSkeleton = styled.div`
  height: 380px;
  border-radius: 4px;
  overflow: hidden;
`;
