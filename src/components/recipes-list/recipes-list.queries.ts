import { gql } from "@apollo/client";

export const GET_RECIPES_LIST = gql`
  query GetRecipesList {
    recipeCollection {
      items {
        title
        photo {
          url
        }
        sys {
          id
        }
      }
    }
  }
`;
