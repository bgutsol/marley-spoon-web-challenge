import React from "react";
import { useQuery } from "@apollo/client";
import { GET_RECIPES_LIST } from "./recipes-list.queries";
import { IRecipesListData } from "./recipes-list.types";
import Link from "next/link";
import { Card } from "../card";
import {
  RecipesListContainer,
  RecipesListItem,
  RecipesListLink,
  RecipesListCardSkeleton,
} from "./recipes-list.styles";
import { SkeletonPlaceholder } from "../skeleton-placeholder";
import { ErrorInformer } from "../error-informer";

export const RecipesList: React.FC = () => {
  const { loading, data, error } = useQuery<IRecipesListData>(GET_RECIPES_LIST);

  if (loading) {
    return (
      <RecipesListContainer>
        {new Array(4).fill(null).map((_, index) => (
          <RecipesListItem key={index}>
            <RecipesListCardSkeleton>
              <SkeletonPlaceholder />
            </RecipesListCardSkeleton>
          </RecipesListItem>
        ))}
      </RecipesListContainer>
    );
  }

  if (error || !data) {
    return (
      <ErrorInformer title="Something went wrong. Try to reload the page" />
    );
  }

  return (
    <RecipesListContainer>
      {data.recipeCollection.items.map((item) => (
        <RecipesListItem key={item.sys.id} data-test-id="recipe">
          <Link href={`/recipe/${item.sys.id}`} passHref>
            <RecipesListLink>
              <Card title={item.title} imgSrc={item.photo.url} />
            </RecipesListLink>
          </Link>
        </RecipesListItem>
      ))}
    </RecipesListContainer>
  );
};
