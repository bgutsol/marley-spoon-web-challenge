import styled from "styled-components";

export const TagContainer = styled.div`
  display: inline-block;
  background-color: #f6f6f6;
  color: #716d6a;
  font-size: 1rem;
  font-weight: 600;
  letter-spacing: 0.05em;
  line-height: 1.5;
  padding: 0.5rem;
  text-transform: uppercase;
`;
