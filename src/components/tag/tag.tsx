import React from "react";
import { TagContainer } from "./tag.styles";
import { ITagProps } from "./tag.types";

export const Tag: React.FC<ITagProps> = (props) => (
  <TagContainer>{props.label}</TagContainer>
);
