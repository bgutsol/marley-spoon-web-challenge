import Link from "next/link";
import { Logo, HeaderContainer, HeaderInner } from "./header.styles";

export const Header: React.FC = () => (
  <HeaderContainer>
    <HeaderInner>
      <Link href="/">
        <Logo src="/logo-martha-marley-spoon.svg" alt="Martha & Marley Spoon" />
      </Link>
    </HeaderInner>
  </HeaderContainer>
);
