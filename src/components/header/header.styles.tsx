import styled from "styled-components";

export const HeaderContainer = styled.div`
  box-shadow: 0 1px 1px -1px rgba(0, 0, 0, 0.1);
  width: 100%;
  background: #fff;
  flex-shrink: 0;
`;

export const HeaderInner = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  max-width: 1120px;
  height: 64px;
  margin: 0 auto;
  padding: 0 20px;
`;

export const Logo = styled.img`
  width: 98px;
  height: 44px;
  cursor: pointer;
`;
