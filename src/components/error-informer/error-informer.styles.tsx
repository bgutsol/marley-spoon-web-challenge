import styled from "styled-components";

export const ErrorInformerContainer = styled.div`
  max-width: 660px;
  margin: 36px auto 73px;
  box-shadow: 0 2px 5px rgba(102, 102, 101, 0.2);
  background-color: #fff;
  border-radius: 4px;
  overflow: hidden;
`;

export const ErrorInformerInfo = styled.div`
  padding: 30px;
  text-align: center;
`;

export const ErrorInformerTitle = styled.h2`
  font-size: 2.2rem;
`;
