import React from "react";
import { IErrorInformerProps } from "./error-informer.types";
import {
  ErrorInformerContainer,
  ErrorInformerInfo,
  ErrorInformerTitle,
} from "./error-informer.styles";

export const ErrorInformer: React.FC<IErrorInformerProps> = (props) => (
  <ErrorInformerContainer>
    <img src="/error.jpg" alt={props.title} />
    <ErrorInformerInfo>
      <ErrorInformerTitle>{props.title}</ErrorInformerTitle>
    </ErrorInformerInfo>
  </ErrorInformerContainer>
);
