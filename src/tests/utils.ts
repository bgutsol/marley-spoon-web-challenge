import { act } from "react-dom/test-utils";
import { mount } from "enzyme";

export const wait = async (amount = 0) => {
  return new Promise((resolve) => setTimeout(resolve, amount));
};

export const actWait = async (amount = 0) => {
  // @ts-ignore
  await act(async () => {
    await wait(amount);
  });
};

export const updateWrapper = async (
  wrapper: ReturnType<typeof mount>,
  amount = 0
) => {
  // @ts-ignore
  await act(async () => {
    await wait(amount);
    wrapper.update();
  });
};
