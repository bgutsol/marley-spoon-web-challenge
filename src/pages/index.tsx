import { RecipesList } from "../components/recipes-list";
import { GET_RECIPES_LIST } from "../components/recipes-list";
import { initializeApollo } from "../lib/apollo-client";

export default function Home() {
  return <RecipesList />;
}

export async function getStaticProps() {
  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: GET_RECIPES_LIST,
  });

  return {
    props: {
      initialApolloState: apolloClient.cache.extract(),
    },
    revalidate: 1,
  };
}
