import { useRouter } from "next/router";
import { RecipeDetails } from "../../components/recipe-details";

const Recipe: React.FC = () => {
  const router = useRouter();

  return <RecipeDetails recipeId={router.query.id as string} />;
};

export default Recipe;
