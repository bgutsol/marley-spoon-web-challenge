import { ErrorInformer } from "../components/error-informer";

export default function Home() {
  return <ErrorInformer title="404 - Page not found" />;
}
