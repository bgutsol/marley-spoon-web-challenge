import { ApolloProvider } from "@apollo/client";
import { useApollo } from "../lib/apollo-client";
import { AppProps } from "next/app";
import { Page } from "../components/page";

export default function App({ Component, pageProps }: AppProps) {
  const apolloClient = useApollo(pageProps.initialApolloState);

  return (
    <ApolloProvider client={apolloClient}>
      <Page>
        <Component {...pageProps} />
      </Page>
    </ApolloProvider>
  );
}
