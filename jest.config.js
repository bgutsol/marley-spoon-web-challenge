module.exports = {
  setupFilesAfterEnv: ["<rootDir>/src/tests/setup-tests.ts"],
  collectCoverageFrom: [
    "**/*.{ts,tsx}",
    "!**/*.d.ts",
    "!**/*.types.ts",
    "!**/*.queries.ts",
    "!**/*.styles.tsx",
    "!**/lib/**",
    "!**/node_modules/**",
    "!**/tests/**",
  ],
  testPathIgnorePatterns: ["/node_modules/", "/.next/"],
  transform: {
    "^.+\\.(ts|tsx)$": "babel-jest",
  },
  transformIgnorePatterns: ["/node_modules/"],
};
