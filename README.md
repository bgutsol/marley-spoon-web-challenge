## Build Setup

```bash
# install dependencies
npm install

# serve dev at localhost:3000
npm run dev

# build for production
npm run build

# start a Next.js production server
npm start
```

## What is done (besides main requirements)

- Ssg for index page (with recipes list)
- ErrorInformer component for errors representing
- ErrorBoundary component for UI errors collecting
- SkeletonPlaceholder component for loading state
- Basic Unit/Snapshot tests
- Basic UI responsiveness

## What could be done next

- Add better tests covarage, change common snapshot tests to more meaningful/specific tests
- Add end-to-end and integration tests (Cypress)
- Add performance checking (web vitals) (Lighthouse)
- Add accessibility checking (Pa11y)
- Separate simple UI components from main project (design system components library)
- Add storybook
- Add types autogeneration based on GraphQL schema (graphql-code-generator)
- Add memoization (`React.memo/PureComponent`) to components that rerenders too often with the same props
- Add babel plugin to remove `data-test-id` attr from prod build

## Used libs

- [NextJs](https://nextjs.org/)
- [Apollo Client](https://www.apollographql.com/docs/react/)
- [Styled Components](https://styled-components.com/)
- [Jest](https://jestjs.io/)
- [Enzyme](https://enzymejs.github.io/enzyme/)
- [react-markdown](https://github.com/rexxars/react-markdown#readme)
